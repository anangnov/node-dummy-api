# dummy-api

### Quick Start

Install package modules
        
    npm install

Run application
    
    node index.js

## License

[MIT](LICENSE)