const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const morgan = require("morgan");
const cors = require("cors");
const json = require("./data");

require("dotenv").config();

const app = express();
// app.use(cors(corsOptions));
// app.use(cors());

const allowedOrigins = ['www.example1.com',
                      'www.example2.com'];
app.use(cors({
  origin: function(origin, callback){
    if(!origin) return callback(null, true);
    if(allowedOrigins.indexOf(origin) === -1){
      const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
}));

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Add custom request
app.use((req, res, next) => {
  next();
});

app.get("/api/data-json", cors(), (req, res, next) => {
  res.status(200).send({
    message: "Success",
    data: json,
    total: json.length
  });
});

const port = process.env.APP_PORT || 8000;
app.set("port", port);

// Listen App
const server = http.createServer(app);
server.listen(port);

// Start Environment
console.log(
  "======================================================================"
);
console.log("Name: " + process.env.APP_NAME);
console.log("Running: " + "http://" + process.env.NODE_HOST + ":" + port);
console.log(
  "======================================================================"
);

module.exports = app;
